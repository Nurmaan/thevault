package thevaultfx;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

/**
 *
 * @author nurma
 */
public class PasswordManagement {// handlng of password object
    private String[] passwords;
    private String[] identifiers;
    private long[] modified;
    private long[] created;
    private boolean[] strong;
    private final Encrypt encrypt;
    private final Suggest sugg;
    private final SArray<Password> pasds = new SArray(100);
    
    public PasswordManagement(){// instantiate fields
        encrypt = new Encrypt();     
        sugg = new Suggest(passwords, identifiers);
        passwords = new String[0];
        identifiers = new String[0];
        retrieveAll();//get all saved data at start
    }
    
    public Password[] getPasswords(){// get the password array
        return this.pasds.toArray(new Password[pasds.size()]);
    }
    
    public String[] getAllPasswords(){// return only the password Strings
        return this.passwords;
    }
    
    public String[] getAllIdentifiers(){// return only the password Strings
        return this.identifiers;
    }
    
    public long[] getAllModified(){// return only the modified longs
        return this.modified;
    }
    
    public String getPassword(String id){// return a password of the identifeir given
        int pos = Arrays.asList(identifiers).indexOf(id);
        if(pos < 0) return "";
        return passwords[pos];
    }
    
    public Password getPasswordObject(String id){// return the password object of the identifeir given
        for (int i = 0; i < pasds.size(); i++) {
            if (pasds.get(i).getIdentifier().equals(id)) {
                return pasds.get(i);
            }
        }
        
        return null;
    }
    
    public String print(String id){// display all the data for a password
        return getPasswordObject(id).print();
    }
    
    private void retrieveAll(){// get all the stored data
        pasds.clear();
        String all = encrypt.decrypt("File\\data.pswdMg");
        String pswds = "", ids = "", createds = "", modifieds = "", strongs = "";
        String[] data = all.split("\\r?\\n");
        if(data.equals("")) return;
        for(int i = 0; i < data.length; i++){
            String[] content = data[i].split("mftZTjNsrB");
            if(content.length == 5){
                if(content[0].equals("")) continue;
                pswds += content[0] + (i < data.length - 1 ? "mftZTjNsrB" : "");
                ids += content[1] + (i < data.length - 1 ? "mftZTjNsrB" : "");
                createds += content[2] + (i < data.length - 1 ? "mftZTjNsrB" : "");  
                modifieds += content[3] + (i < data.length - 1 ? "mftZTjNsrB" : "");  
                strongs += content[4] + (i < data.length - 1 ? "mftZTjNsrB" : ""); 

                pasds.add(new Password(content[0], content[1], Long.parseLong(content[2]), Long.parseLong(content[3])));
                
            }
        }
        passwords = pswds.split("mftZTjNsrB");
        identifiers = ids.split("mftZTjNsrB");
        created = stringToLong(createds.split("mftZTjNsrB"));
        modified = stringToLong(modifieds.split("mftZTjNsrB"));        
        strong = stringToBoolean(strongs.split("mftZTjNsrB")); 
        sugg.setData(passwords, identifiers);
    }
    
    private long[] stringToLong(String[] input){// convert string to long[]
        long[] ret = new long[input.length];
        for(int i = 0; i < ret.length; i ++){
            if(!input[i].equals("")) ret[i] = Long.parseLong(input[i]);
        }
        
        return ret;
    }
    
    private boolean[] stringToBoolean(String[] input){// convert string to boolean[]
        boolean[] ret = new boolean[input.length];
        for(int i = 0; i < created.length; i ++){
            ret[i] = Boolean.parseBoolean(input[i]);
        }
        
        return ret;
    }
    
    public boolean isDuplicate(String id){//check if the identifier already exists
        boolean ret = false;
        for (int i = 0; i < pasds.size(); i++) {
            if (pasds.get(i) != null && pasds.get(i).getIdentifier().equals(id)) {
                ret = true;
                break;
            }
        }
        return ret;
    }
    
    public void storeChangedPassword(String password, String identifier, boolean pass, String newId){
        int pos = Arrays.asList(identifiers).indexOf(identifier);// store a changed password, either new password or new identifier
        if(pass){
            pasds.get(pos).setPassword(password);
        }else{
            pasds.get(pos).setIdentifier(newId);
        }
        
        pasds.get(pos).setModified();
        modified[pos] = pasds.get(pos).getModified();
        save("");
    }
    
    
    
    public String[] recoverPassword(String[] ids, String[] pass){ // call passwor recovery class     
        PasswordRecovery recover = new PasswordRecovery(passwords, identifiers);
        return recover.recover(ids, pass);
    }
    
    public void removePassword(String id){ // remove a password   
        int passIndex = Arrays.asList(identifiers).indexOf(id);
        String pass = passwords[passIndex];        
        String content = "";        
        for(int i = 0; i < passwords.length; i++){
            if(i != passIndex){
                content += passwords[i] + "mftZTjNsrB" + identifiers[i] + "mftZTjNsrB" + created[i] + "mftZTjNsrB" + modified[i] + "mftZTjNsrB" + strong[i] + System.lineSeparator();
            }
        }
        
        save(content);
    }
    
    public String generatePassword(boolean sp, boolean num, int length){// generate a password
        String ret = "";
        String[] alphabets = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", 
            "x", "y", "z"};
        
        String[] capAlphabets = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", 
            "X", "Y", "Z"};
        
        String[] special = {"<", ">", "?", ":", "{", "}", "|", "+", "_", ")",
            "(", "*", "&", "^", "%", "$", "#", "@", "!", "~", "`", "-", "=", "\\",
            "[", "]", ";", "'", ",", ".", "/"};
        
        String[] number = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};
            
        length = length > 100 ? 100 : length; // amx length to 100
        Object[] workingArray; // array of selected charaters

        if(sp && num){ // creating working array
            workingArray = Stream.of(alphabets, capAlphabets, special, number).flatMap(Stream::of).toArray();
        }else if(sp){
            workingArray = Stream.of(alphabets, capAlphabets, special).flatMap(Stream::of).toArray();
        }else if(num){
            workingArray = Stream.of(alphabets, capAlphabets, number).flatMap(Stream::of).toArray();
        }else{
            workingArray = Stream.of(alphabets, capAlphabets).flatMap(Stream::of).toArray();
        }

        String[] wkArr = new String[workingArray.length];
        System.arraycopy(workingArray, 0, wkArr, 0, workingArray.length);

        Random rand = new Random();
        boolean state = true;
        String password = "";
        boolean createPass = true;
        password = "";
        for(int i = 0; i < length; i++){// create string
            int numb = rand.nextInt(wkArr.length);
            password += wkArr[numb];
        }
        ret = password;

           
        return ret;
    }
    
    private String save(String content){// write all password object attributes to file
        TheVaultFX.clearData("File\\data.pswdMg");
        if(content.equals("")){      
            for(int i = 0; i < pasds.size(); i++){
                content += pasds.get(i).getPassword()+ "mftZTjNsrB" + pasds.get(i).getIdentifier() + "mftZTjNsrB" + pasds.get(i).getTime()+ "mftZTjNsrB" + pasds.get(i).getModified() + "mftZTjNsrB" + pasds.get(i).isStrong() + (i < pasds.size() ? System.lineSeparator() : "");
            }
        }
        encrypt.write(encrypt.directEncrypt(content.getBytes()), "File\\data.pswdMg");// encrypting and writing
        retrieveAll();
        return content;
    }
    
    public void createPassword(String password, String identifier){// create new password
        pasds.add(new Password(password, identifier, System.currentTimeMillis(), System.currentTimeMillis()));
        save("");

    }
    
    public SArray<String> passwordToIdentifier(SArray<String> password){ // get identifier of password
        SArray<String> identifier = new SArray(password.size());
        for(int i = 0; i < password.size(); i++){
            identifier.add(identifiers[Arrays.asList(passwords).indexOf(password.get(i))]);
        }
        
        return identifier;
    }
    
    public void openDocumentation(){ //open documentation 
        try {
           Desktop desktop = Desktop.getDesktop();
           File myFile = new File("res//documentation.docx");
           desktop.open(myFile);
        } catch (IOException ex) {
        }    
    }
    
}
