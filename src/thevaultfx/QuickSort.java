package thevaultfx;

import java.util.Random;

/**
 *
 * @author nurma
 */
public class QuickSort {// quicksort algorithm
    
    public QuickSort(){
        
    }
    
    public static Object[][] sort(Object[][] input){
        //input in the format: [[sum, index], [sum, index], ...]
        
        quickSort(input, 0, input.length - 1);
        return input;
    }
    
    public static void quickSort(Object[][] A, int low, int high) {
        if (low < high+1) {
            int p = partition(A, low, high);
            quickSort(A, low, p-1);
            quickSort(A, p+1, high);
        }
    }

    private static void swap(Object[][] A, int index1, int index2) {
        Object[] temp = A[index1];
        A[index1] =  A[index2];
        A[index2] = temp;
    }

    // returns random pivot index between low and high inclusive.
    private static int getPivot(int low, int high) {
        Random rand = new Random();
        return rand.nextInt((high - low) + 1) + low;
    }

    // moves all n < pivot to left of pivot and all n > pivot 
    // to right of pivot, then returns pivot index.
    private static int partition(Object[][] A, int low, int high) {
        swap(A, low, getPivot(low, high));
        int border = low + 1;
        for (int i = border; i <= high; i++) {
            String type = A[i][0].getClass().toString();
            switch(type){
                case "class java.lang.Long":
                    if ((Long.parseLong(A[i][0].toString()) < Long.parseLong(A[low][0].toString()))) {
                        swap(A, i, border++);
                    }
                   break;
                case "class java.lang.Integer":
                    if ((Integer.parseInt(A[i][0].toString()) < Integer.parseInt(A[low][0].toString()))) {
                        swap(A, i, border++);
                    }
                   break;
                case "class java.lang.String":
                    if (A[i][0].toString().compareTo(A[low][0].toString()) < 0) {
                        swap(A, i, border++);
                    }
                   break;
            }
            
        }
        swap(A, low, border-1);
        return border-1;
    }
}
