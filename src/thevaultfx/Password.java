package thevaultfx;

/**
 *
 * @author nurma
 */
public class Password {
    private final Long time;
    private Long modified;
    private String identifier;
    private String password;
    private boolean strong;
    
    public Password(String password, String identifier, Long created, Long modified){// password object storing all the password information
        this.identifier = identifier;
        this.password = password;
        this.time = created == null ? System.currentTimeMillis() : created;
        this.modified = modified == null ? this.time : modified;
        checkStrong(this.password);
    }
    
    private void checkStrong(String pass){// test if password is strong based on characters present and length
        int special = 0, nonCapped = 0, capped = 0, num = 0;
        for(int i = 0; i < pass.length(); i++){
            if(((int)pass.charAt(i)) <= 122 && ((int)pass.charAt(i)) >= 97){
                nonCapped = 1;
            }else if((((int)pass.charAt(i)) <= 47 && ((int)pass.charAt(i)) >= 33 )|| (((int)pass.charAt(i)) <= 64 && ((int)pass.charAt(i)) >= 59) || (((int)pass.charAt(i)) <= 96 && ((int)pass.charAt(i)) >= 91) || (((int)pass.charAt(i)) <= 126 && ((int)pass.charAt(i)) >= 123)){
                special = 1;
            }else if(((int)pass.charAt(i)) <= 90 && ((int)pass.charAt(i)) >= 65){
                capped = 1;
            }else if(((int)pass.charAt(i)) <= 48 && ((int)pass.charAt(i)) >= 57){
                num = 1;
            }
        }
        
        if(special + nonCapped + capped + num >= 2 && pass.length() > 8){
            this.strong = true;
        }
    }
    
    public boolean isStrong(){// returns password strength
        return this.strong;
    }
    
    public void setPassword(String password){// change the password
        this.password = password;
        setModified();// set modified time to now
    }
    
    public void setIdentifier(String identifier){// change the identifier
        this.identifier = identifier;
        setModified();// set the modified time to now
    }
    
    public String getPassword(){// returns the password
        return this.password;
    }
    
    public String getIdentifier(){// returns the identifier
        return this.identifier;
    }
    
    public String print(){// returns all the data in a proper String to display
        return "PASSWORD\t" + this.password + "\nIDENTIFIER\t" + this.identifier + "\nCREATED ON\t" + new java.util.Date(this.time) 
                + "\nMODIFIED ON\t" + new java.util.Date(this.modified)
                + "\nRATING\t" + (this.strong ? "Strong" : "Weak");
    }
    
    public Long getTime(){// returns the creation time
        return this.time;
    }
    
    public Long getModified(){// returns the modification time
        return this.modified;
    }
    
    public void setModified(){// sets the modification time to now
        this.modified = System.currentTimeMillis();
    }
    
    
}
