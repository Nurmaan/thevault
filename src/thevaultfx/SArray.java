package thevaultfx;

import java.util.Collection;

/**
 *
 * @author nurma
 */
public class SArray<T> { // Scalable Array Class which allows the user to have a 'Collection'-type array which can be scaled to any size on adding new items
    private int length;
    private int count = 0;
    private Object[] obj;
    
    public SArray(int length){// constructor that sets the length of obj
        this.length = length;
        obj = new Object[length];
    }
    
    public SArray (){// constructor that gives default length to obj
        length = 10;
        obj = new Object[length];
    }
    
    public void add(T newObj){// adding a new object of class T to the array
        if(count >= length - 1) newSpace(1);
        obj[count++] = newObj;
    }
    
    public int size(){// returns the filled size of obj
        return count;
    }
    
    public T get(int index){// returns a particular element at index 'index' from obj
        return (T)obj[index];
    }
    
    private void newSpace(int len){// creates a 'len' number of new spaces in the obj array
        Object[] newObj = new Object[length + len];
        for(int i = 0; i < length; i++){
            newObj[i] = obj[i];
        }
        obj = newObj;
        length += len;
    }
    
    public void set(int index, T newObj){// assigns the object newObj to the index 'index' of obj
        if(index >= 0 && index < length){
            obj[index] = newObj;
        }else{
            System.out.println("set Failed with index " + index);
        }
    }
    
    public void addAll(Collection<T> c){// add all the contents of collection to obj
        int newLength = c.size();
        int diff = newLength - (length - count);
        if(diff > 0) newSpace(diff);
        c.forEach(val->{
            add(val);
        });
        
    }
    
    public boolean isEmpty(){// check if obj is empty
        return count == 0;
    }
    
    public T[] toArray(T[] arr){// returns an array with all the filled contents of obj
        for(int i = 0; i < count; i++){
            arr[i] = (T)obj[i];
        }
        
        return arr;
    }
    
    public void clear(int length){// clears all the elements from obj and set the new length of the new obj
        obj = new Object[length];
        count = 0;
        this.length = length;
    }
    
    public void clear(){// clears all the elemnts from obj and create new obj with default length
        obj = new Object[10];
        count = 0;
        length = 1;
    }
    
    public String[][] toStringArray(){// returns a String array from all the contents in obj (specific use for suggest)
        String[][] ret = new String[count][2];
        for(int i = 0; i < count; i++){
            SArray inner = (SArray)obj[i];
            ret[i][0] = inner.get(0).toString();
            ret[i][1] = inner.get(1).toString();

        }
        return ret;
    }
    
    public void toSArray(Object[][] input){// sets the values of obj based on input array (specific use for suggest)
        obj = new Object[input.length];
        for(int i = 0; i < input.length; i++){
            SArray sa = new SArray(2);
            sa.add(input[i][0]);
            sa.add(input[i][1]);
            obj[i] = sa;
        }
    }
}
