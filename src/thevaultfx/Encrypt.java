package thevaultfx;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author nurma
 */
public class Encrypt {// encryption class
    

    public Encrypt(){
    }
    
    private byte[] fetch(String fileName){// read and return the bytes from a file at fileName
        byte[] data = null;
        try {
            Path location = Paths.get(fileName);
            data = Files.readAllBytes(location);

        } catch (IOException ex) {
        }        
        return data;
    }
    
    public byte[] directEncrypt(byte[] data){// encrypt the bytes from the byte array data to a new encrypted byte array
        
        for(int i = 0; i < data.length; i++){// simple mathematical encryption
            data[i] = (byte) (data[i] +(123*i) - ((data.length - i) * 123));                
        }
        return data;
    }

    public String decrypt(String fileName){// decrypt the data from fileName
        byte[] data = fetch(fileName);// get the bytes from the location
        
        for(int i = 0; i < data.length; i++){// decryption
            data[i] = (byte) (data[i] -(123*i) + ((data.length - i) * 123));
        }
        return new String(data);
    }
    
    public void write(byte[] output, String file){// write the byte array output to the file 'file'
        FileOutputStream fos = null;
        try {            
            fos = new FileOutputStream(file);
            fos.write(output);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
            }
        }
    }
}
