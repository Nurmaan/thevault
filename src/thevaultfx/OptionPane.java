/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thevaultfx;

import java.util.Arrays;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author nurma
 */
public class OptionPane {// self-made javafx version of JOptionPane
    
    private final int minWidth = 250;
    private final int maxWidth = 700;
    private final Stage primaryStage;
    private int ret = -1;
    private String str = "";
    
    public OptionPane(Stage stage){// sets final field value in constructor
        this.primaryStage = stage;
    }
    
    public int getChoice(String msg, String[] options, boolean mandatory){// offers user multiple choices
        ret = -1;
        
        Label message = new Label(msg);
        VBox vbox = new VBox(15);
        HBox buttonBox = new HBox(10);
        for(int  i = 0; i < options.length; i++){
            Button btn = new Button(options[i]);
            buttonBox.getChildren().add(btn);
            btn.setOnAction(value->{
                ret = Arrays.asList(options).indexOf(value.getSource().toString().substring(value.getSource().toString().indexOf("'")).replace("'", ""));
                ((Node)(value.getSource())).getScene().getWindow().hide();
                primaryStage.show();
            });
        }
        vbox.getChildren().addAll(message, buttonBox);
        vbox.setPadding(new Insets(20,20,20,20));
        vbox.setAlignment(Pos.CENTER);
        vbox.setMinWidth(minWidth);
        vbox.setMaxWidth(maxWidth);
        
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setMinWidth(minWidth);
        buttonBox.setMaxWidth(maxWidth);
        
        message.setWrapText(true);
        message.setMaxWidth(maxWidth);

        

        Scene scene = new Scene(vbox);
        scene.getStylesheets().add("default.css");
        Stage stage = new Stage();
        stage.setTitle("The Vault");
        stage.setScene(scene);
        stage.setMinWidth(minWidth);
        stage.setMaxWidth(maxWidth);
        stage.setOnCloseRequest(value->{
            primaryStage.show();
        });
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        if(mandatory) primaryStage.hide();
        stage.getIcons().add(new Image("file:res\\logo1.png"));
        stage.showAndWait();
        
        return ret;
    }
    
    public String getString(String msg, boolean secret, boolean mandatory){// simple messagebox 
        str = "";
        Label message = new Label(msg);
        VBox vbox = new VBox(15);
        HBox hbox = new HBox();
        Button confirmButton  = new Button("Ok");
        TextField text = new TextField();
        PasswordField  password = new PasswordField();
        
        vbox.setPadding(new Insets(20,20,20,20));
        vbox.setAlignment(Pos.CENTER);
        vbox.setMinWidth(minWidth);
        vbox.setMaxWidth(maxWidth);
                
        message.setWrapText(true);
        message.setMaxWidth(maxWidth);
        
        confirmButton.setOnAction(value->{
            str = secret ? password.getText() : text.getText();
            ((Node)(value.getSource())).getScene().getWindow().hide();
            primaryStage.show();
        });
        
        text.setOnAction(value->{
            str = secret ? password.getText() : text.getText();
            ((Node)(value.getSource())).getScene().getWindow().hide();
            primaryStage.show();
        });
        
        password.setOnAction(value->{
            str = secret ? password.getText() : text.getText();
            ((Node)(value.getSource())).getScene().getWindow().hide();
            primaryStage.show();
        });
        
        
        hbox.getChildren().add(confirmButton);
        hbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(message, secret ? password : text, hbox);
        

        Scene scene = new Scene(vbox);
        scene.getStylesheets().add("default.css");
        Stage stage = new Stage();
        stage.setTitle("The Vault");
        stage.setScene(scene);
        stage.setMinWidth(minWidth);
        stage.setMaxWidth(maxWidth);
        stage.setOnCloseRequest(value->{
            primaryStage.show();
        });
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        if(mandatory) primaryStage.hide();
        stage.getIcons().add(new Image("file:res\\logo1.png"));
        stage.showAndWait();
        return str;
    }
    
    public void showMessage(String msg, boolean mandatory){// just notify message. user response does not matter

        Label message = new Label(msg);
        VBox vbox = new VBox(15);
        
        vbox.setPadding(new Insets(20,20,20,20));
        vbox.setAlignment(Pos.CENTER);
        vbox.setMinWidth(minWidth);
        vbox.setMaxWidth(maxWidth);
                
        message.setWrapText(true);
        message.setMaxWidth(maxWidth);
        
        vbox.getChildren().addAll(message);
        
        message.setOnMousePressed(value->{            
            ((Node)(value.getSource())).getScene().getWindow().hide();
            primaryStage.show();
        });
        
        vbox.setOnMouseClicked(value->{            
            ((Node)(value.getSource())).getScene().getWindow().hide();
            primaryStage.show();
        });
        
        
        Scene scene = new Scene(vbox);
        scene.getStylesheets().add("default.css");
        Stage stage = new Stage();
        stage.setTitle("The Vault");
        stage.setScene(scene);
        stage.setMinWidth(minWidth);
        stage.setMaxWidth(maxWidth);
        stage.setOnCloseRequest(value->{
            primaryStage.show();
        });
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        if(mandatory) primaryStage.hide();
        stage.getIcons().add(new Image("file:res\\logo1.png"));
        stage.showAndWait();
    }
    
    public int showContent(Node node, boolean mandatory, String param, View view){// display the node
        ret = 0;
        VBox vbox = new VBox();
        vbox.getChildren().add(node);
        Scene scene = new Scene(vbox);
        scene.getStylesheets().add("default.css");
        Stage stage = new Stage();
        stage.setTitle("The Vault");
        stage.setScene(scene);
        stage.setMinWidth(minWidth);
        stage.setMaxWidth(maxWidth);
        stage.setOnCloseRequest(value->{
            primaryStage.show();
        });
        stage.setResizable(false);
        stage.setAlwaysOnTop(true);
        stage.setOnCloseRequest(value->{
            ret = 1;
        });
        if(mandatory) primaryStage.hide();
        stage.setOnCloseRequest(value->{
            view.closedWindow(param);
        });
        stage.getIcons().add(new Image("file:res\\logo1.png"));
        stage.showAndWait();
        return ret;
    }
    
}
