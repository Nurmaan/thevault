package thevaultfx;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 *
 * @author nurma
 */
public class View { // deals with all GUIs
    private final OptionPane resp;
    private final Stage stage;
    private PasswordManagement pass;
    private String retString;
    private boolean create, recover, generate;
    
    public View(Stage stage){//constructor
        this.stage = stage;
        this.resp = new OptionPane(stage);
    }
    
    public GridPane main(PasswordManagement pass, String[] identifiers){ // drawing the main node
        this.pass = pass;
        int numCols = 5 ;
        int colWidth = 100;
        boolean id = identifiers != null;
        Password[] password = pass.getPasswords();
        GridPane pane = new GridPane();
        pane.setPrefWidth(numCols * colWidth);

        int col = 0;
        int row = 0;
        int limit = !id ? pass.getAllPasswords().length : identifiers.length;
            
        if(!id) TheVaultFX.setSuggestions(pass.getAllIdentifiers());
        
        for(int i = 0; i < limit; i++){ // create a button for each identifier being displayed
            Button btn = new Button(!id ?  password[i].getIdentifier() : identifiers[i]);
            btn.setPrefSize(colWidth, colWidth);
            btn.getStyleClass().add("allButton");
            btn.setTooltip(new Tooltip(id ? identifiers[i] : password[i].getIdentifier()));
            btn.setContextMenu(getMainContext(pass, i, btn));
            btn.setOnAction(value->{
                String str = value.getSource().toString().substring(value.getSource().toString().indexOf("'")).replace("'", "");
                String pswd = pass.getPassword(str);
                resp.showMessage(pswd, false);
                copyToClipboard(pswd);
            });
            if(col < numCols){
                col++;
            }else{
                col = 1;
                row++;
            }
            pane.addRow(row, btn);
        }
        return pane;
    }
    
    private ContextMenu getMainContext(PasswordManagement pass, int pos, Button btn){ // create the context menu for the identifier buttons
        ContextMenu menu = new ContextMenu();
        MenuItem item4 = new MenuItem("View Data");
        MenuItem item1 = new MenuItem("Edit password");
        MenuItem item2 = new MenuItem("Edit identifier");
        MenuItem item3 = new MenuItem("Remove");
        item4.setOnAction(value -> {
            parseAction("data", btn.getText(), pass);
        });
        item1.setOnAction(value -> {
            parseAction("editPassword", btn.getText(), pass);
        });
        item2.setOnAction(value -> {
            parseAction("editIdentifier", btn.getText(), pass);
        });
        item3.setOnAction(value -> {
            parseAction("removePassword", btn.getText(), pass);
        });
        
        menu.getItems().addAll(item4, item1);
        if(!btn.getText().equals("master")) menu.getItems().addAll(item2, item3); // master cannot be removed or identifier changed
        
        return menu;

    }
    
    private void parseAction(String param, String name, PasswordManagement pass){ // action handler for context menu
        switch(param){
            case "editPassword":
                editPassword(name, pass);
                break;
            case "removePassword":
                removePassword(name, pass);
                break;
            case "editIdentifier":
                editIdentifier(name, pass);
                break;
            case "data":
                showData(name, pass);
                break;
        }
    }
    
    private void editPassword(String name,  PasswordManagement pass){ // alter the password
        if(name.equals("master")){ // changing master password
            VBox vbox = new VBox(15);
            PasswordField oldPass = new PasswordField(), newPass = new PasswordField(), repeatPass = new PasswordField();
            Button confirm = new Button("Confirm"), generate = new Button("Generate");
            Label alert = new Label("");
            
            
            alert.getStyleClass().add("alert");
            alert.setWrapText(true);
            alert.setMaxWidth(250);
            
            oldPass.setPromptText("Current Password");
            newPass.setPromptText("New Password");
            repeatPass.setPromptText("Repeat New Password");
            vbox.setPadding(new Insets(10,10,10,10));
            vbox.getChildren().addAll(oldPass, newPass, repeatPass, confirm, generate, alert);
            vbox.setAlignment(Pos.CENTER);
            vbox.setPrefWidth(300);
            confirm.setOnAction(value -> { // button pressed
                if(oldPass.getText().equals(pass.getPassword("master")) && !oldPass.getText().equals("") && !newPass.getText().equals("") && !repeatPass.getText().equals("")){ //sanity check
                    if(newPass.getText().equals(repeatPass.getText())){ //both paswsords have to match
                        pass.storeChangedPassword(newPass.getText(), "master", true, null);                                   
                        ((Node)(value.getSource())).getScene().getWindow().hide();
                        TheVaultFX.refresh();
                    }else{
                        alert.setText("Passwords do not match");
                    }
                }else{
                    alert.setText("Check your input fields");
                }
            });
            
            repeatPass.setOnAction(value -> { // same as above
                if(oldPass.getText().equals(pass.getPassword("master")) && !oldPass.getText().equals("") && !newPass.getText().equals("") && !repeatPass.getText().equals("")){
                    if(newPass.getText().equals(repeatPass.getText())){
                        pass.storeChangedPassword(newPass.getText(), "master", true, null);                                   
                        ((Node)(value.getSource())).getScene().getWindow().hide();
                        TheVaultFX.refresh();
                    }else{
                        alert.setText("Passwords do not match");
                    }
                }else{
                    alert.setText("Check your input fields");
                }
            });
            
            generate.setOnAction(value->{// generate the new password
                String password = generate(true);
                newPass.setText(password);
                repeatPass.setText(password);

            });
            
            
            resp.showContent(vbox, false, "", this);
            
        }else{ // changing normal password
            
            VBox vbox = new VBox(15);
            PasswordField password = new PasswordField();
            Label alert = new Label("");
            HBox hbox = new HBox(20);
            Button generate = new Button("generate"), confirm = new Button("Confirm");
            
            password.setPromptText("New Password");
            
            alert.getStyleClass().add("alert");
            alert.setWrapText(true);
            alert.setMaxWidth(250);
                        
            hbox.getChildren().addAll(generate, confirm);
            hbox.setAlignment(Pos.CENTER);
            vbox.setAlignment(Pos.CENTER);
            vbox.getChildren().addAll(password, hbox, alert);
            
            generate.setOnAction(value->{// generating new password
                password.setText(generate(true));
            });
            
            confirm.setOnAction(value->{// button pressed
                 if(!password.getText().equals("")){
                    pass.storeChangedPassword(password.getText(), name, true, null);                                   
                    ((Node)(value.getSource())).getScene().getWindow().hide();
                    TheVaultFX.refresh();
                }else{
                    alert.setText("I can't save no password");
                }
            });
            
            resp.showContent(vbox, false, "", this);
        }
        
    }
    
    private void removePassword(String name, PasswordManagement pass){ // remove a password
        if(!name.toLowerCase().equals("master")){// cannot remove master
            int ret = resp.getChoice("Is this a good idea?", new String[]{"Yes", "No"}, false); //check whether the user really wants to remove this password
            if(ret == 0){
                pass.removePassword(name);
                TheVaultFX.refresh();
            }
        }
       
    }
    
    private void editIdentifier(String name, PasswordManagement pass){// edit the identifier
        if(name.equals("master")){// cannot change the identifier of master
            resp.showMessage("Sorry you cannot change the master identifier", true);
        }else{
            Password password = pass.getPasswordObject(name);
            String newId = resp.getString("What will the new identifier be?", false, false); // get new identifier
            if(!password.getIdentifier().equals(newId) && !newId.equals("")){
                pass.storeChangedPassword(password.getPassword(), password.getIdentifier(), false, newId);
                TheVaultFX.refresh();
            }
        }
        
    }
    
    private void showData(String name, PasswordManagement pass){// show all the attributes for a password
        resp.showMessage(pass.print(name), false);
    }
    
    private void display(Node node){// set the center of the main screen in TheVaultFX GUI
       thevaultfx.TheVaultFX.setCenterContent(node);
    }
    
    public void add(){// create a new password
        if(!create) resp.showContent(addView(), false, "create", this);// check if window create already open
    }
    
    public VBox addView(){// the add screen designer
        create = true;
        VBox vbox = new VBox();
        HBox identifierBox = new HBox();
        HBox passwordBox = new HBox();
        HBox confirmBox = new HBox();
        Button confirmButton = new Button("Create");
        Label identifierLabel = new Label("Identifier"), passwordLabel = new Label("Password"), alert = new Label("");
        PasswordField passwordField = new PasswordField();
        TextField identifierField = new TextField();
        
        alert.getStyleClass().add("alert");
        alert.setWrapText(true);
        alert.setMaxWidth(250);
        
        identifierBox.getChildren().addAll(identifierLabel, identifierField);
        passwordBox.getChildren().addAll(passwordLabel, passwordField);
        confirmBox.getChildren().add(confirmButton);
        confirmBox.setAlignment(Pos.CENTER); 
        identifierBox.setAlignment(Pos.CENTER);
        passwordBox.setAlignment(Pos.CENTER);
        identifierBox.setPadding(new Insets(10,10,10,10));
        passwordBox.setPadding(new Insets(10,10,10,10));
        vbox.setPadding(new Insets(10,10,10,10));
        identifierLabel.setPadding(new Insets(10,10,10,10));
        passwordLabel.setPadding(new Insets(10,10,10,10));
        
        confirmButton.setOnAction(value->{ // confirm button pressed
            String password = passwordField.getText();
            String identifier = identifierField.getText();
            if(!pass.isDuplicate(identifier) && !identifier.equals("") && !password.equals("")){// sanity check
                pass.createPassword(password, identifier);
                TheVaultFX.refresh();
                passwordField.setText("");
                identifierField.setText("");
                alert.setText("");
                identifierField.requestFocus();//set fous back to identifier field
            }else{
                alert.setText("Please check your identifier and password. Identifiers have to be unique");
            }
        });
        
        passwordField.setOnAction(value->{ // same as above
            String password = passwordField.getText();
            String identifier = identifierField.getText();
            if(!pass.isDuplicate(identifier) && !identifier.equals("") && !password.equals("")){
                pass.createPassword(password, identifier);
                TheVaultFX.refresh();
                passwordField.setText("");
                identifierField.setText("");
                alert.setText("");
                identifierField.requestFocus();
            }else{
                alert.setText("Please check your identifier and password. Identifiers have to be unique");
            }
        });
        


        vbox.setPrefHeight(75*8 - 60);
        vbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(identifierBox, passwordBox, confirmBox, alert);
        return vbox;
    }
    
    public String generate(boolean once){// generate password screen
        if(once || !generate){// check if window not yet opened or opening only once for generating password when eidting it
            if(!once)generate = true;
            retString = "";
            VBox vbox = new VBox(20);
            CheckBox sp = new CheckBox("Include Special Characters"), num = new CheckBox("Include Numbers");
            TextField identifier = new TextField(), length = new TextField();
            Label generated = new Label("Click on Generate to start"), alert = new Label(), identifierLabel = new Label();
            identifier.setPromptText("Identifier");
            length.setPromptText("Length (greater than 8)");
            Button confirm = new Button("Confirm"), generate = new Button("Generate");
            HBox checkH = new HBox(15), buttonH = new HBox(15);

            alert.getStyleClass().add("alert");
            length.setOnKeyReleased(value->{// check whether user is inputing a number
                try{
                    Integer.parseInt(length.getText());
                    alert.setText("");
                }catch(NumberFormatException e){
                    alert.setText("The length has to be a number");
                }
            });

            generate.setOnAction(value->{// generate buttin pressed
                if(!alert.getText().equals("The length has to be a number") && !length.getText().equals("")){// sanity check
                    int len = Integer.parseInt(length.getText());
                    if(len > 8){// minimum length for a fair password     
                        String id = identifier.getText();
                        boolean special = sp.isSelected(), number = num.isSelected();
                        String text = pass.generatePassword(special, number, len);
                        generated.setText(text);
                        generated.setTooltip(new Tooltip(text));
                        alert.setText("");
                    }else{
                        alert.setText("Length Should be greater than 8");
                    }
                }else{
                    alert.setText("Please fill in the fields properly");
                }
            });

            confirm.setOnAction(value->{
                if((once || !identifier.getText().equals("")) && !generated.getText().equals("Click on Generate to start")){// sanity check.. no identifier is required for once
                    String id = identifier.getText();
                    String password = generated.getText();
                    if(!pass.isDuplicate(id)){
                        if(!once)pass.createPassword(password, id);
                        TheVaultFX.refresh();
                        identifier.setText("");
                        length.setText("");
                        sp.setSelected(false);
                        num.setSelected(false);
                        generated.setText("Click on Generate to start");
                        alert.setText("");
                        retString = password;
                        copyToClipboard(password);
                        if(once)((Node)(value.getSource())).getScene().getWindow().hide();
                    }else{
                        alert.setText("It looks like you're trying to duplicate an identifier");
                    }
                }else{
                    alert.setText("Unable to create password");
                }
            });

            checkH.getChildren().addAll(sp, num); 
            buttonH.getChildren().addAll(confirm, generate);
            buttonH.setAlignment(Pos.CENTER);
            generated.setWrapText(true);
            if(!once) vbox.getChildren().add(identifier);
            vbox.getChildren().addAll(length, checkH, generated, buttonH, alert);
            vbox.setAlignment(Pos.CENTER);
            vbox.setMaxWidth(400);
            vbox.setPadding(new Insets(10,10,10,10));
            if(once){
                resp.showContent(vbox, false, "", this);
            }else{
                resp.showContent(vbox, false, "generate", this);
            }

            return retString;
        }
        return "";
        
    }
    
    public void closedWindow(String param){// keep track of what windows have been closed so they can be opened again
        switch(param){
            case "create":
                create = false;
                break;
            case "recover":
                recover = false;
                break;
            case "generate":
                generate = false;
                break;
        }
    }
    
    public void recover(){// recover a lost password
        if(!recover){ // check whether window is opened already
            recover = true; // let  the application know that the recover window is visible
            
            //designing the recover window
            ScrollPane scroll = new ScrollPane();
            VBox vbox = new VBox(20);
            HBox identifierH = new HBox(10), passwordH = new HBox(10);
            Button identifierB = new Button("+"), passwordB = new Button("+"), confirm = new Button("Confirm");
            Label alert = new Label("");
            SArray<TextField> identifiers = new SArray();
            SArray<TextField> passwords = new SArray();

            alert.getStyleClass().add("alert");
            alert.setWrapText(true);
            alert.setMaxWidth(450);
            alert.setAlignment(Pos.CENTER);

            TextField identifierT = new TextField();
            identifierT.setPromptText("Identifier 1");
            identifiers.add(identifierT);

            TextField passwordT = new TextField();
            passwordT.setPromptText("Password 1");
            passwords.add(passwordT);

            FlowPane identifierF = new FlowPane(identifierT);
            FlowPane passwordF = new FlowPane(passwordT);

            identifierH.getChildren().addAll(identifierF, identifierB);
            passwordH.getChildren().addAll(passwordF, passwordB);

            identifierB.setOnAction(value->{ // create a new textfield for identifeirs
                TextField tf = new TextField();
                identifiers.add(tf);
                tf.setPromptText("Identifier " + identifiers.size());
                identifierF.getChildren().add(tf);
            });

            passwordB.setOnAction(value->{ // create a new textfield for passwords
                TextField tf = new TextField();
                passwords.add(tf);
                tf.setPromptText("Password " + passwords.size());
                passwordF.getChildren().add(tf);
            });

            confirm.setOnAction(value->{ // confirm button pressed
                SArray<String> ids = new SArray(), pas = new SArray();
                for(int i = 0; i < identifiers.size(); i++){ // get all identifiers
                    String identifier = identifiers.get(i).getText();
                    if(!identifier.equals("")){
                        ids.add(identifier);
                    }
                }
                
                for(int i = 0; i < passwords.size(); i++){ // get all passwords
                    String password = passwords.get(i).getText();
                    if(!password.equals("")){
                        pas.add(password);
                    }
                }

               if(ids.isEmpty() && pas.isEmpty()){ //  sanity check
                    alert.setText("Please provide at least an identifier and a password");
                }else{
                    String[] arr = pass.recoverPassword(ids.toArray(new String[ids.size()]), pas.toArray(new String[pas.size()]));// call password recovery class
                    display(main(pass, arr));   // display all the identifiers that correspond to the recovery
                }
            });

            vbox.getChildren().addAll(identifierH, passwordH, confirm, alert);
            vbox.setPadding(new Insets(10,10,10,10));
            scroll.setContent(vbox);
            scroll.setPrefHeight(500);

            resp.showContent(scroll, false, "recover", this);
        }
       
    }
    
    public void login(PasswordManagement pass, Stage prim){ // login to the application
        String master = pass.getPassword("master");
        if(master.equals("")){ // create new master password for first time use
            VBox vbox = new VBox(15);
            PasswordField newPass = new PasswordField(), repeatPass = new PasswordField();
            Button confirm = new Button("Confirm");
            Label alert = new Label("");
            alert.getStyleClass().add("alert");
            alert.setWrapText(true);
            alert.setMaxWidth(450);
            alert.setAlignment(Pos.CENTER);
            newPass.setPromptText("New Master Password");
            repeatPass.setPromptText("Repeat Master Password");
            vbox.setPadding(new Insets(10,10,10,10));
            vbox.getChildren().addAll(newPass, repeatPass, confirm, alert);
            vbox.setAlignment(Pos.CENTER);
            confirm.setOnAction(value->{// confirm button pressed
                if(newPass.getText().equals(repeatPass.getText()) && !newPass.equals("")){ // sanity check
                    pass.createPassword(newPass.getText(), "master");
                    ((Node)(value.getSource())).getScene().getWindow().hide();
                    prim.show();
                    display(main(pass, null));
                    if(!pass.getPasswords()[0].isStrong()) resp.showMessage("Beware! Your master password is currently weak. You MUST change it", true);
                }else{
                    alert.setText("Passwords do not match");
                    newPass.setText("");
                    repeatPass.setText("");
                }
            });
            
            repeatPass.setOnAction(value->{// same as above
                if(newPass.getText().equals(repeatPass.getText()) && !newPass.equals("")){
                    pass.createPassword(newPass.getText(), "master");
                    ((Node)(value.getSource())).getScene().getWindow().hide();
                    prim.show();
                    display(main(pass, null));
                    if(!pass.getPasswords()[0].isStrong()) resp.showMessage("Beware! Your master password is currently weak. You MUST change it", true);
                }else{
                    alert.setText("Passwords do not match");
                    newPass.setText("");
                    repeatPass.setText("");
                }
            });
            
            
            if(resp.showContent(vbox, true, "", this) == 1){
                System.exit(0);
            }
            
        }else{// provide master password
            String password = resp.getString("Please Type In The Master Password", true, true);
            if(password.equals(master)){
                display(main(pass, null));
                if(!pass.getPasswords()[0].isStrong()) resp.showMessage("Beware! Your master password is currently weak. You MUST change it", true);
            }else{
                resp.showMessage("Incorrect Password. Shutting Down", true);
                System.exit(0);
            }
        }
        
    }
    
    public void settings(){
        VBox vbox = new VBox(15);
        HBox h1 = new HBox(15);
        Label lMaster = new Label("Remind to change weak master password");
        //ToggleSwitch tsMaster = new ToggleSwitch();
    }
    
    
    
    public void copyToClipboard(String msg) {// copy text o clipboard
        while (true) {
            try {
                StringSelection stringSelection = new StringSelection(msg);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(stringSelection, null);
                break;
            } catch (IllegalStateException e) {

            }
        }
    }
}
