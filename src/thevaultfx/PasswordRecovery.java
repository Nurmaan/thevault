package thevaultfx;

import java.util.Arrays;

/**
 *
 * @author nurma
 */
public class PasswordRecovery {// recover password algo
    
    private String[] passwords;
    private String[] identifiers;
    private Suggest sugg;
    
    public PasswordRecovery(String[] passwords, String[] identifiers){// constructor setting the passwords and identifiers used to suggest
        this.passwords = passwords;
        this.identifiers = identifiers;
        this.sugg = new Suggest(passwords, identifiers);
        sugg.setData(this.passwords, this.identifiers);
    }
    
    public void setData(String[] passwords, String[] identifiers){// updating the values of passwords and identifiers
        this.passwords = passwords;
        this.identifiers = identifiers;
        this.sugg = new Suggest(passwords, identifiers);
        sugg.setData(this.passwords, this.identifiers);
    }
    
    public String[] recover(String[] ids, String[] pass){// return identifiers based on identifiers and passwords remembered
        SArray<String> ret = new SArray(), idSugg = new SArray(), passSugg = new SArray();
        if(ids.length>0){// identifiers provided
            for (String id : ids) {
                SArray<String> resp = sugg.suggest(id, false, 2);// getting suggested identifiers
                String[] resps = resp.toArray(new String[resp.size()]);
                idSugg.addAll(Arrays.asList(resps));
            }

            ret = idSugg;  
        }
        
        
        if(pass.length > 0){// passwords provided
            if(ret.isEmpty()){// identifeirs have not been provided
                for (String pas : pass) {
                    SArray<String> resp = sugg.suggest(pas, true, 7);// getting suggested passwords
                    String[] resps = resp.toArray(new String[resp.size()]);
                    passSugg.addAll(Arrays.asList(resps));
                }
                
                for(int i = 0; i < passSugg.size(); i++){
                    ret.add(identifiers[Arrays.asList(passwords).indexOf(passSugg.get(i))]);// adding identifier of password
                } 
            }else{                
                for (String pas : pass) {
                    SArray<String> sug = sugg.suggestPasswordFilter(pas, ret, 10); // getting all the identifiers who's password ressemble the String password pas within an error of 10
                    passSugg.addAll(Arrays.asList(sug.toArray(new String[sug.size()])));
                }
                
                ret = passSugg;
            }
            
        }
        
        return  ret.toArray(new String[ret.size()]);
    }
}
