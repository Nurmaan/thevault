package thevaultfx;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author nurma
 */
public class TheVaultFX extends Application {

    static PasswordManagement pass;
    static View view;
    static Suggest sugg;
    private static Node centerContent;
    private static ScrollPane center;
    private static SArray<String> suggestions;

    @Override
    public void start(Stage primaryStage) {//javafx start method
        createFile("File\\data.pswdMg"); //create files incase it's the first run or files have been removed to prevent file reader exceptions later
        createFile("File\\passIndex.pswdMg");
        createFile("File\\idIndex.pswdMg");
        
        //initialise fields
        pass = new PasswordManagement();
        sugg = new Suggest();
        view = new View(primaryStage);
        
        //drawing the applciation
        BorderPane root = new BorderPane();
        VBox top = new VBox();
        HBox searchBox = new HBox();
        ChoiceBox searchType = new ChoiceBox(), sortType = new ChoiceBox();
        TextField search = new TextField();
        
        searchType.getItems().addAll("Search by Identifier", "Search by Password");
        sortType.getItems().addAll("Sort by Identifier", "Sort by Date Modified");

        searchType.setValue("Search by Identifier");
        sortType.setValue("Sort by Identifier");

        searchBox.getChildren().addAll(search, searchType, sortType);
        top.getChildren().addAll(menu(), searchBox);
        search.setPromptText("Search");
        
        search.setOnKeyReleased(value -> { //on search
            String srch = search.getText();
            if (srch.length() > 0) { // no use of looking for nothing
                sugg.setData(pass.getAllPasswords(), pass.getAllIdentifiers()); // preparing the suggest class
                boolean password = true;
                if(searchType.getValue().toString().contains("Identifier")) password = false; // if user search for identifeir
                suggestions = sugg.suggest(srch, password, 1); 
                if(password) suggestions = pass.passwordToIdentifier(suggestions);
                sort(sortType); // sort results from suggestion
            } else if (srch.length() == 0) { // user wants to see everything again
                refresh();
            }
        });
        
        searchType.setOnAction(value->{ //reapeat of the same function as search
           String srch = search.getText();
            if (srch.length() > 0) {
                sugg.setData(pass.getAllPasswords(), pass.getAllIdentifiers());
                boolean password = true;
                if(searchType.getValue().toString().contains("Identifier")) password = false;
                suggestions = sugg.suggest(srch, password, 1);
                if(password) suggestions = pass.passwordToIdentifier(suggestions);
                sort(sortType);
            } else if (srch.length() == 0) {
                refresh();
            }
        });
        
        sortType.setOnAction(value->{
            sort(sortType);
        });
        
        root.setTop(top);
        center = new ScrollPane();

        center.setPrefHeight(75 * 8);
        center.setPrefWidth(400);
        root.setCenter(center);

        Scene scene = new Scene(root);
        scene.getStylesheets().add("default.css"); //styling

        primaryStage.setResizable(false);
        primaryStage.setHeight(75 * 8);
        primaryStage.setWidth(5*100);
        primaryStage.setTitle("The Vault");
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(value -> {
            System.exit(0);
        });
        primaryStage.show();        
        primaryStage.getIcons().add(new Image("file:res\\logo1.png"));
        view.login(pass, primaryStage);
    
    }
    
    public void sort(ChoiceBox sortType){// sort the data beign displayed
        if (sortType.getValue().toString().contains("Date")) { //sort by date modified
            String[] display = new String[suggestions.size()];
            Password[] pasds = pass.getPasswords();
            String[] identifiers = pass.getAllIdentifiers();
            SArray<SArray> data = new SArray(suggestions.size());
            for (int i = 0; i < suggestions.size(); i++) {
                SArray couple = new SArray();
                couple.add(pasds[Arrays.asList(identifiers).indexOf(suggestions.get(i))].getModified());
                couple.add(i);
                data.add(couple);
            }
            data.toSArray(QuickSort.sort(data.toStringArray()));
            for (int i = 0; i < data.size(); i++) {
                display[i] = suggestions.get(Integer.parseInt(data.get(i).get(1).toString()));
            }

            setCenterContent(view.main(pass, display));
        } else if (sortType.getValue().toString().contains("Identifier")) { //sort by identifier
            String[] display = new String[suggestions.size()];
            SArray<SArray> data = new SArray(suggestions.size());
            for (int i = 0; i < suggestions.size(); i++) {
                SArray couple = new SArray();
                couple.add(suggestions.get(i));
                couple.add(i);
                data.add(couple);
            }
            data.toSArray(QuickSort.sort(data.toStringArray()));
            for (int i = 0; i < data.size(); i++) {
                display[i] = suggestions.get(Integer.parseInt(data.get(i).get(1).toString()));
            }

            setCenterContent(view.main(pass, display));
        }
    }
    
    public static void setSuggestions(String[] identifiers){ // externally set the data to be displayed
        suggestions = new SArray();
        suggestions.addAll(Arrays.asList(identifiers));
    }
    
    public static void refresh() {
        center.setContent(view.main(pass, null));
    }

    public static void setCenterContent(Node node) { // externally set the center node
        TheVaultFX.centerContent = node;
        center.setContent(centerContent);
    }

    private HBox menu() { // draw the upper menu
        String[] tooltips = new String[]{"Home", "Create", "Generate Strong Password", "Recover", "Help"};
        HBox hbox = new HBox();
        for (int i = 0; i < 5; i++) {
            hbox.getChildren().add(item(String.valueOf(i + 1), tooltips[i]));
        }
        hbox.setStyle("-fx-background-color:#bbbca9");
        hbox.setAlignment(Pos.CENTER);
        return hbox;
    }

    private VBox item(String icon, String tooltip) { // draw individual buttons 
        Image image = new Image(TheVaultFX.class.getResourceAsStream("/icon/" + icon + ".png"));
        ImageView imageview = new ImageView(image);
        Button btn = new Button();
        btn.setGraphic(imageview);
        btn.setTooltip(new Tooltip(tooltip));
        btn.setPrefSize(60, 60);
        btn.getStyleClass().add("menuBtn");
        btn.getStyleClass().add(tooltip);
        btn.setOnAction(value -> {
            actionHandler(value);
        });
        Pane paneIndicator = new Pane();
        paneIndicator.setPrefSize(60, 10);
        paneIndicator.setStyle("-fx-background-color:#bbbca9");
        menuDecorator(btn, paneIndicator);
        VBox vbox = new VBox(btn, paneIndicator);
        return vbox;
    }

    private void menuDecorator(Button btn, Pane pane) { //menu button animation on hover
        btn.setOnMouseEntered(value -> {
            btn.setStyle("-fx-background-color:#8e8e81");
            pane.setStyle("-fx-background-color:#707070");
        });

        btn.setOnMouseExited(value -> {
            btn.setStyle("-fx-background-color:#bbbca9");
            pane.setStyle("-fx-background-color:#bbbca9");
        });
    }

    private void actionHandler(ActionEvent e) { //handles menu button clicks
        String val = e.toString();
        if (val.contains("Create")) {
            view.add();
        }else if (val.contains("Generate")) {
            view.generate(false);
        }else if(val.contains("Recover")){
            view.recover();
        }else if(val.contains("Home")){
            setCenterContent(view.main(pass, null));
        }else if(val.contains("Help")){
            pass.openDocumentation();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    private static void createFile(String fileName) { // creates a new file if not exists at the fileName
        File f = new File(fileName);
        f.getParentFile().mkdirs();
        try {
            FileWriter fr = new FileWriter(f, true);

        } catch (IOException ex) {
            Logger.getLogger(TheVaultFX.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void clearData(String file){ // erases all the data in the file
        FileWriter fr = null;
        try {
            File f = new File(file);
            fr = new FileWriter(f, false);
            fr.write("");
        } catch (IOException ex) {
            Logger.getLogger(Password.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fr.close();
            } catch (IOException ex) {
                Logger.getLogger(Password.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
}
