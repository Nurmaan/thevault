package thevaultfx;

import java.util.Arrays;

/**
 *
 * @author nurma
 */
public class Suggest {// suggests passwords or identifiers based on keybaord layout
    private String[] passwords;
    private String[] identifiers;
    private final Encrypt encrypt = new Encrypt();
    private final String[] alphabets = 
        {"`", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=",
        "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "\\",
        "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'",
        "z", "x", "c", "v", "b", "n", "m", ",", ".", "/",
        "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+",
        "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "|",
        "A", "S", "D", "F", "G", "H", "J", "K", "L", ":", "\"", "Z", "X", "C", "V", "B", "N", "M", "<", ">", "?"," "};
    private final int[] indexes = 
        {1,1,2,2,3,3,4,4,5,5,6,6,7,
        2,2,3,3,4,4,5,5,6,6,7,7,8,
        3,3,4,4,5,5,6,6,7,7,8,
        4,4,5,5,6,6,7,7,8,8,
        5,5,6,6,7,7,8,8,9,9,10,10,11,
        6,6,7,7,8,8,9,9,10,10,11,11,12,
        7,7,8,8,9,9,10,10,11,11,12,
        8,8,9,9,10,10,11,11,12,12,13};
    private int[] passIndex, passSum, idIndex, idSum;
    
    public Suggest(){// default constructor
    }
    
    public Suggest(String[] passwords, String[] identifiers){ // setting fields in constructor
        this.passwords = passwords;
        this.identifiers = identifiers;
    }
    
    public void setData(String[] passwords, String[] identifiers){ // setting identifiers and passwords for proper suggestion
        this.passwords = passwords;
        this.identifiers = identifiers;
        compile(true);
        compile(false);
        retrieve(true);
        retrieve(false);
    }
    
    public void compile(boolean password){ // geenrating the sum of each identifier and passwords and saving it
        SArray<SArray<Integer>> content = new SArray(passwords.length);
        for(int i = 0; i < passwords.length; i++){
            SArray<Integer> sumIndex = new SArray(2);
            sumIndex.add(StringToInt(password ? passwords[i] : identifiers[i]));
            sumIndex.add(i);
            content.add(sumIndex);
        }
        content.toSArray(QuickSort.sort(content.toStringArray()));
        String print = "";
        for(int i = 0; i < content.size(); i++){
            print += content.get(i).get(0) + ";" + content.get(i).get(1) + (i != content.size() - 1 ? System.lineSeparator() : "");
        }
        TheVaultFX.clearData(password ? "File\\passIndex.pswdMg" : "File\\idIndex.pswdMg");
        encrypt.write(encrypt.directEncrypt(print.getBytes()), password ? "File\\passIndex.pswdMg" : "File\\idIndex.pswdMg");
    }
    
    public void retrieve(boolean password){ // getting all the indexes fromthe fiels that have been saved
        String all = encrypt.decrypt(password ? "File\\passIndex.pswdMg" : "File\\idIndex.pswdMg");
        String[] data = all.split("\\r?\\n");
        int[] index = new int[data.length];
        int[] sum = new int[data.length];
        for(int i = 0; i < data.length; i++){
            String[] within = data[i].split(";");
            if(within.length == 2){
                sum[i] = Integer.parseInt(within[0]);
                index[i] = Integer.parseInt(within[1]);
            }
        }
        
        if(password){
            passIndex = index;
            passSum = sum;
        }else{
            idIndex = index;
            idSum = sum;
        }
        
    }
    
    public SArray<String> suggest(String input, boolean password, int degree){ // suggest a password or identifier that best represent the input String with an error of the Integer degree
        //if password => password has to be suggested
        SArray<String> ret = new SArray();
        int val  = StringToInt(input);
        for(int i = 0; i < passSum.length; i++){
            if((password && passwords[passIndex[i]].contains(input)) || (!password && identifiers[idIndex[i]].contains(input))){
                ret.add(password ? passwords[passIndex[i]] : identifiers[idIndex[i]]);
            }else if((password ? passSum[i] : idSum[i]) <= val + degree && (password ? passSum[i] : idSum[i]) >= val - degree){
                ret.add(password ? passwords[passIndex[i]] : identifiers[idIndex[i]]);
            }
        }
        return ret;
    }
    
    private int StringToInt(String input){ // getting the index for a String, adding the number equivalent fro each character of that String
        int sum = 0;
        for(int j = 0; j < input.length(); j++){
           sum += indexes[Arrays.asList(alphabets).indexOf(Character.toString(input.charAt(j)))];
        } 
        return sum;
    }
    
    public SArray<String> suggestPasswordFilter(String pas, SArray<String> ids, int degree){ // returns all the identifiers who's password ressemble the String password pas within the error degree
        SArray<String> chosen = new SArray();
        int val = StringToInt(pas);
        
        for(int i = 0; i < ids.size(); i++){
            String currentPassword = passwords[Arrays.asList(identifiers).indexOf(ids.get(i))];
            int newVal = StringToInt(currentPassword);
            if ((val <= newVal + degree && val >= newVal - degree) || currentPassword.contains(pas)) {
                chosen.add(ids.get(i));
            }
        }
        return chosen;
    }
}
